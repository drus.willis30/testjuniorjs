import Vue from "vue";
import Vuex from 'vuex'
import employees from "../pages/Employees";

Vue.use(Vuex);

const defaultEmployee = { // создаем объект сотрудника по умолчанию
    name: "",
    sName: "",
    mName: "",
    eMail: "",
    id: 0,
    friends: [],
}
export default new Vuex.Store({
    state: {
        employees: [
            {
                name: "Юрий",
                sName: "Долгорукий",
                mName: "Евгеньевич",
                eMail: "dolgorukii@ya.ru",
                id: 1,
                friends: [],
            },
            {
                name: "Андрей",
                sName: "Семенов",
                mName: "Александрович",
                eMail: "semen@ya.ru",
                id: 2,
                friends: [],
            },
            {
                name: "Федор",
                sName: "Попов",
                mName: "Юрьевич",
                eMail: "popov@ya.ru",
                id: 3,
                friends: [],
            },
            {
                name: "Евгений",
                sName: "Долгорукий",
                mName: "Евгеньевич",
                eMail: "dolgorukiievgen@ya.ru",
                id: 4,
                friends: [],
            },
            {
                name: "Андрей",
                sName: "Успенский",
                mName: "Сергеевич",
                eMail: "uspas@ya.ru",
                id: 5,
                friends: [],
            },
            {
                name: "Виталий",
                sName: "Федотов",
                mName: "Евгеньевич",
                eMail: "fedot@ya.ru",
                id: 6,
                friends: [],
            },
        ],
        employee: JSON.parse(JSON.stringify(defaultEmployee)),
    },
    mutations: {
        deleteItem(state) {
            const itemIndex = state.employees.map(employee => employee.id).indexOf(state.employee.id) // находим индек удаляемого элемента
            state.employees.splice(itemIndex, 1) // удаляем элемент
            state.employees.map(item => {
                const deleteMeFromFriends = item.friends.indexOf(state.employee.id) // смотрим есть ли сотрудник в друзьях у других сотрудников
                if (deleteMeFromFriends > -1) item.friends.splice(deleteMeFromFriends, 1) // если есть удаляем
                return item
            })
            state.employee = JSON.parse(JSON.stringify(defaultEmployee)) // очищаем объект сотрудника
        },
        fillEmployee(state, payload = null) { // заполняем объект данными
            if (!payload) {
                state.employee = JSON.parse(JSON.stringify(defaultEmployee))
            } else state.employee = JSON.parse(JSON.stringify(payload))
        },
        saveItem(state, payload) { // сохраняем изменения в данных сотрудника
            const itemIndex = state.employees.map(employee => employee.id).indexOf(payload.id) // находим индекс элемента
            if (itemIndex > -1) {
                Object.assign(state.employees[itemIndex], payload) // если элемент найден заменяем его
            } else {
                payload.id = Date.now()
                state.employees.push(payload) // если не найден создаем новый
            }
            for (let id of state.employee.friends) {
                const empl = state.employees.find(el => el.id === id)
                const itemIndex = empl.friends.indexOf(state.employee.id) // находим индекс удаляемого элемента
                if (itemIndex > -1) {
                    empl.friends.splice(itemIndex, 1) // удаляем дружеские связи другим пользователям
                }
            }
            for (let id of payload.friends) {
                const empl = state.employees.find(el => el.id === id)
                const itemIndex = empl.friends.indexOf(state.employee.id) // находим индекс элемента
                if (itemIndex === -1) {
                    empl.friends.push(payload.id) // добавляем дружеские связи другим пользователям
                }
            }
            state.employee = JSON.parse(JSON.stringify(defaultEmployee)) // очищаем объект сотрудника
        },
    }
});
