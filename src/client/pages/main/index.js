import Vue from "vue";
import router from '../../router'
import Vuetify from "vuetify/lib";
import store from '../../store'

import 'vuetify/dist/vuetify.css';

import Main from './Main';

Vue.use(Vuetify, {iconfont: 'mdi'});

new Vue({
    vuetify: new Vuetify(),
    router,
    store,
    render: (h) => h(Main)
}).$mount('#app');
