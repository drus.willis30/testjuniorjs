import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

const routes = [
    {
        path: '/employees',
        name: 'Main',
        component: () => import('../pages/Employees.vue'),
    },
    {
        path: '/employee/:method',
        name: 'Employee',
        component: () => import('../pages/Employee.vue'),
    },
    {
        path: '*',
        redirect: '/employees'
    },
]
const router = new VueRouter({
    mode: 'history',
    routes
})

export default router
